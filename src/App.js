import React from "react";
import ReactDOM from "react-dom";

const App = () => {
  return (
    <div>
      <div className="btn btn-success">React here!</div>
    </div>
  );
};
export default App;
ReactDOM.render(<App />, document.getElementById("app"));
